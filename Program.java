import java.net.InetSocketAddress;
import java.util.Scanner;

public class Program {
    static Scanner input = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        System.out.println("Name : ");
        String name = input.nextLine();

        System.out.println("Port : ");
        int port = Integer.parseInt(input.nextLine());

        System.out.println("peer Ip : ");
        String peerIp = input.nextLine();

        System.out.println("peer port");
        int peerPort = Integer.parseInt(input.nextLine());

        Channel channel = new Channel();
        channel.bind(port);
        channel.start();

        System.out.println("started");
        InetSocketAddress address = new InetSocketAddress(peerIp , peerPort);
        while (true){
            String msg = input.nextLine();
            if(msg.isEmpty()){
                break;
            }
            msg = name + " >> "+msg;
            channel.sendTo(address,msg);
            System.out.println(msg);
        }
        input.close();
        channel.stop();

        System.out.println("Closed");
    }
}
